#include "fileIO.h"
#include "newton.h"
#include "pavement.h"
#include <iostream>

int main(int argc, char* argv[])
{
    if(argc != 4)
    {
        std::cout << "usage: asphaltKinetics configFileName dataFileName outputFileName" << std::endl;
		exit(1);
    }
    const auto params = getParams(argv[1]);
    const auto temperatures = getData(argv[2]);
	const auto outFileName = argv[3];
    run(params, temperatures, outFileName);
}
