#pragma once
#include "json.hpp"
#include <memory>
#include <fstream>
#include <vector>
using std::vector;
using std::shared_ptr;
class AsphaltConstants;
using json = nlohmann::json;

shared_ptr<AsphaltConstants> getParams(const char* configFileName);
vector<double> getData(const char* dataFileName);
void toFile(const json& data, const char* fileName);
void writeData(const vector<double> values, const vector<double> times, const char* fileName);
