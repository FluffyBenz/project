#pragma once

#include <algorithm>
#include <functional>

using std::function;
using Func = const function<double(const double&)> &;

double step(Func f, Func df, const double x) noexcept;

double findRootNewton(Func f, Func df, const double x0, const double tolerance) noexcept;

double invertNewton(Func f, Func df, const double y, const double x0, const double tolerance) noexcept;

double invertBisect(Func f, const double y, const double lower, const double upper, const double tolerance) noexcept;

