
import csv
from numpy import genfromtxt
import json

if __name__ == "__main__":
    my_data = genfromtxt('data.csv', delimiter=',')
    my_col = {'data' : my_data[:,1].tolist()}
    with open('data.json', 'w') as outfile:
        json.dump(my_col, outfile)
    print my_data[:,1]
